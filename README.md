# Shubham Kumar Training

## Scope - JavaScript

TASK 1 :- Branch ~javascript javascript/taskOne/taskOne.js(basic JavaScript)<br/>
TASK 2 :- Branch ~javascript javascript/taskTwo(Javascript classes, prototype, closure)<br/>
TASK 7 :- Branch ~javascript javascript/taskSeven(Javascript question 1, form validation and submission)<br/>
TASK 8 :- Branch ~javascript javascript/taskEight(javascript question 2, person class program)<br/>

## Scope NodeJS

Task 10 :- Branch ~node TaskTen(Node js express app)<br/>
Task 11 :- Branch ~node TaskEleven(Cats api program)<br/>
Task 12 :- Branch ~node TaskTweleve(Typescript programs)<br/>
Task 13 :- Branch ~node TaskThirteen(NestJS Tutorial Task Management Application with TYPEORM)<br/>
Task 14 :- Branch ~node TaskFourteen(Cats api program using NestJS, TypeORM and Sqlite database)
